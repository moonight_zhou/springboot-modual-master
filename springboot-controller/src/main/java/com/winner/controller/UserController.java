package com.winner.controller;

import com.winner.annotation.SystemControllerLog;
import com.winner.utils.IDUtils;
import com.winner.pojo.User;
import com.winner.service.UserService;
import com.winner.common.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/show")
    public String show() {
        return "show";
    }

    @RequestMapping(value = "findAll", method = RequestMethod.GET)
    @SystemControllerLog(description = "查询用户信息",actionType = "4")
    @ResponseBody
    public ResultVO<List<User>> findAll() {

        return new ResultVO(200,"请求成功",userService.findAll());
    }

    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    @ResponseBody
    public String insert() {
        User user = new User();
        user.setId(IDUtils.uuid());
        user.setUsername("张三");
        user.setPassword("123");
        userService.insert(user);
        return "insert success";
    }

}
