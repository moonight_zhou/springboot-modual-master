package com.winner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@ComponentScan(basePackages = "com.winner")
@SpringBootApplication
public class SpringbootControllerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootControllerApplication.class, args);
	}

	@RequestMapping(value = "/", method= RequestMethod.GET)
	public Map<String, Object> showIndex() {
		Map<String, Object> map = new HashMap<>();
		map.put("Project", "SpringBootMutil");
		map.put("Author", "lzz");
		map.put("Date", "2018-05-23");
		return map;
	}

}
