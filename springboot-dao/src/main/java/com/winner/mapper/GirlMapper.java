package com.winner.mapper;

import com.winner.pojo.Girl;
import com.winner.pojo.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface GirlMapper {
    void insert(Girl girl);

    List<Girl> findAll();
}
