package com.winner.utils;

import java.util.UUID;

public class IDUtils {
    public static String uuid() {
        return UUID.randomUUID().toString().replace("-", "").toUpperCase();
    }
}
