package com.winner.service;

import com.winner.pojo.Girl;
import com.winner.pojo.User;

import java.util.List;

public interface GirlService {
    void insert(Girl girl);

    List<Girl> findAll();
}
