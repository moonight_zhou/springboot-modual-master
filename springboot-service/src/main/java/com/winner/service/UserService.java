package com.winner.service;

import com.winner.pojo.User;

import java.util.List;

public interface UserService {
    void insert(User user);

    List<User> findAll();
}
