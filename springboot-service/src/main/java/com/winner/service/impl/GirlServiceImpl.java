package com.winner.service.impl;

import com.winner.mapper.GirlMapper;
import com.winner.pojo.Girl;
import com.winner.service.GirlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GirlServiceImpl implements GirlService {

    @Autowired
    private GirlMapper girlMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insert(Girl girl) {
        girlMapper.insert(girl);
    }

    @Override
    public List<Girl> findAll() {
        return girlMapper.findAll();
    }
}
