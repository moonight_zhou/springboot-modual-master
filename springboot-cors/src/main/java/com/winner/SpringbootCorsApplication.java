package com.winner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.winner")
public class SpringbootCorsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootCorsApplication.class, args);
	}
}
