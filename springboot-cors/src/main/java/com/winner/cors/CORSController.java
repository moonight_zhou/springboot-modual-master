package com.winner.cors;

import com.winner.common.ResultVO;
import com.winner.pojo.Girl;
import com.winner.service.GirlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/cors")
public class CORSController {
    @Autowired
    private GirlService girlService;

    @RequestMapping(value = "findAll", method = RequestMethod.GET)
    @ResponseBody
    public ResultVO<List<Girl>> findAll() {

        return new ResultVO(200,"请求成功",girlService.findAll());
    }
}
